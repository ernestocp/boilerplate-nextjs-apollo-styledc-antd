import { gql, useQuery } from '@apollo/client';

const TEST_QUERY = gql`
  query TEST_QUERY{
    categories {
      id
      cover
      name
      emoji
      path
    }
  }
`;

function useTestQuery() {
  const data = useQuery(TEST_QUERY);
  return data;
}

export { useTestQuery, TEST_QUERY };
