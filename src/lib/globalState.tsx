import React, { createContext, useContext, useState } from 'react';

const LocalStateContext = createContext({});
const LocalStateProvider = LocalStateContext.Provider;

function GlobalStateProvider({ children }: any) {
  // This is our own custom provider! We will store data (state) 
  // and functionality (updaters) in here and anyone can access it via the consumer!

  // Closed cart by default
  const [cartOpen, setCartOpen] = useState(false);

  function toggleCart() {
    console.log('working')
    setCartOpen(!cartOpen);
  }

  function closeCart() {
    setCartOpen(false);
  }

  function openCart() {
    setCartOpen(true);
  }

  return (
    <LocalStateProvider
      value={{
        cartOpen,
        setCartOpen,
        toggleCart,
        closeCart,
        openCart,
      }}
    >
      {children}
    </LocalStateProvider>
  );
}

// make a custom hook for accessing the local state
function useGlobal() {
  // We use a consumer here to access the local state
  const all = useContext(LocalStateContext);
  return all;
}
export { GlobalStateProvider, useGlobal };
