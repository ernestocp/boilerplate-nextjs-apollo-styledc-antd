import { ApolloProvider } from '@apollo/client';
import 'antd/dist/antd.css';
import { AppProps } from 'next/app';
import Router from 'next/router';
import NProgress from 'nprogress';
import Page from '../components/Page';
import '../components/styles/nprogress.css';
import { GlobalStateProvider } from '../lib/globalState';
import withData from '../lib/withData';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps, apollo }: AppProps | any) {
  return (
    <ApolloProvider client={apollo}>
      <GlobalStateProvider>
        <Page>
          <Component {...pageProps} />
        </Page>
      </GlobalStateProvider>
    </ApolloProvider>
  );
}

MyApp.getInitialProps = async function ({ Component, ctx }: any) {
  let pageProps: any = {};
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }
  pageProps.query = ctx.query;
  return { pageProps };
};

export default withData(MyApp);