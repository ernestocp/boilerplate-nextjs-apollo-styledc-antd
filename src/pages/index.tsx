import { Button, Card } from 'antd';
import Link from 'next/link';
import { FC } from 'react';
import styled from 'styled-components';
import ErrorMessage from '../components/ErrorMessage';
import { useTestQuery } from '../components/graphql/testQuery';
import { useGlobal } from '../lib/globalState';

const TestStyleH1 = styled.h1`
  color: var(--black);
`;

const TestStyleP = styled(Card)`
  color: var(--red);
  :hover {
    color: palevioletred;
    border-color: palevioletred;
  }
`;

const TestButton = styled(Button)`
  /* color: var(--red); */
`;

const IndexPage: FC = () => {
  const { toggleCart }: any = useGlobal()
  const { data, error, loading} = useTestQuery()
  if (loading) return <p>Loading...</p>;
  if (error) return <ErrorMessage error={error} />;
  return (
    <div>
      <TestStyleH1>Index Page</TestStyleH1>
      <TestButton type='dashed' onClick={toggleCart}>CLICK</TestButton>
      <Link href='/about'>
        About
      </Link>

      {
        data.categories.map((el: any) => (
          <TestStyleP key={el?.id}>{el?.name}</TestStyleP>
        ))
      }
    </div>
  )
}

export default IndexPage