# Boilerplate Typescript-Nextjs-Apollo-StyledC-Antd

Boilerplate of a Typescript Nextjs Project with Apollo, Styled Components and Antd

Based on:

- <https://github.com/wesbos/Advanced-React>
- <https://github.com/vercel/next.js/tree/canary/examples/with-styled-components>
- <https://github.com/vercel/next.js/tree/canary/examples/with-ant-design>
